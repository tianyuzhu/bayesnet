#include <cassert>
#include <iterator>
#include <memory>
#include <utility>
#include <functional>
#include <ostream>

#include "factor.h"
#include "slice_algorithm.h"

using namespace std;
using namespace std::placeholders;

/// Forward Declarations ///

typedef unsigned char vindex;
typedef unsigned int findex;

static inline
findex findexWith(findex f, const vindex v);

static inline
bool notIn(Var var, Slice<Var> slice);

static inline
bool containsAll(Factor const&factor, Slice<Var> vars);

class FVal
{
public:
	FVal(Slice<double>, Slice<Var>, Slice<Var>);
	~FVal();

	double operator()(size_t, size_t);
private:
	Slice<vindex> indicies, sorted;
	Slice<double> vals;
};

static inline
void restrictFactors(
	Slice<Factor> factors,
	Slice<Asst> evidence);

static inline
Slice<Factor> sumoutFactors(
	Slice<Factor> factors, Var var);

static inline void debug(char const *, Factor const&);

/// Factor ///

Factor::Factor() :
_vdata(),
_fdata(),
vars(nullptr, nullptr),
facts(nullptr, nullptr)
{}

Factor::Factor(size_t D) :
_vdata(new Var[D]),
_fdata(new double[1<<D]),
vars(_vdata.get(), D),
facts(_fdata.get(), 1<<D)
{}

void Factor::init(
	size_t D,
	Var const *vars_,
	double const *facts_)
{
	copy(vars_, vars_+D, vars.begin());
	copy(facts_, facts_+(1<<D), facts.begin());
}

Factor::Factor(Factor const&f) :
_vdata(nullptr),
_fdata(nullptr),
vars(f.vars),
facts(f.facts)
{}

Factor::Factor(Factor &&f) :
_vdata(move(f._vdata)),
_fdata(move(f._fdata)),
vars(f.vars),
facts(f.facts)
{}

Factor &Factor::operator =(Factor const&f)
{
	if (&f == this) return *this;
	_vdata.reset(nullptr);
	_fdata.reset(nullptr);
	vars = f.vars;
	facts = f.facts;
	return *this;
}

Factor &Factor::operator =(Factor &&f)
{
	if (f == *this) return *this;
	_vdata.swap(f._vdata);
	_fdata.swap(f._fdata);
	vars = move(f.vars);
	facts = move(f.facts);
	return *this;
}

Factor::Factor(
	Var decision,
	Slice<Var> dependants,
	Slice<Var> hidden,
	Slice<Factor> factors) :
Factor(dependants.size() + 1)
{
	auto vari = copy(dependants, vars.begin());
	*vari = decision;

	// Create initial evidence array
	unique_ptr<Asst[]> ebuf(new Asst[dependants.size()]);
	Slice<Asst> evidence(ebuf.get(), dependants.size());

	vari = dependants.begin();
	for (auto &asst : evidence) {
		asst = ~(*vari++); // Initialize all to false
	}

	// Create two slices for the true and false decisions
	findex mid = facts.size()/2;
	Slice<double>
	decideNo(facts.subslice(mid)),
	decideYes(facts.subslice(mid, facts.size()));

	Var query[] = {decision};
	for (findex i = 0; i < mid; ++i) {
		// Perform inference based on evidence.
		Factor factor(inference(
			factors, query, hidden, evidence));
		assert(factor.vars.size() == 1);

		// Set fact values
		auto f = factor.facts;
		double yes = (f[1] > f[0])? 1 : 0;
		decideYes[i] = yes;
		decideNo[i] = 1-yes;

		// Increment evidence
		for (auto &asst : evidence) {
			asst.val = !asst.val;
			if (asst.val) break;
		}
	}
}

void swap(Factor &f1, Factor &f2)
{
	f1 = move(f2);
}

Factor Factor::restrict(Asst asst)
{
	auto restricted = find(vars, asst.var);
	if (restricted == vars.end()) return *this;

	const vindex v = restricted - vars.begin();
	assert(v < sizeof(findex));
	const findex vbit = asst.val? 1 << v : 0;

	Factor result = factorWithout(restricted);
	findex i = 0;
	for (auto &fact : result.facts) {
		findex f = findexWith(i++, v);
		fact = facts[f | vbit];
	}

	return result;
}

Factor Factor::sumout(Var var)
{
	auto summedOut = find(vars, var);
	if (summedOut == vars.end()) return *this;

	const vindex v = summedOut - vars.begin();
	assert(v < sizeof(findex));
	const findex vbit = 1 << v;

	Factor result = factorWithout(summedOut);
	findex i = 0;
	for (auto &fact : result.facts) {
		findex f = findexWith(i++, v);
		fact = facts[f] + facts[f | vbit];
	}

	return result;
}

void Factor::normalize()
{
	double sum = 0;
	for (double fact : facts)
		sum += fact;
	if (sum == 1 || sum == 0) return;
	for (double& fact : facts)
		fact /= sum;
}

Factor Factor::operator *(Factor const&f)
{
	const vindex D1 = vars.size();
	const vindex D2 = f.vars.size();

	// Calculate the common variables
	size_t maxSize = D1 + D2;
	unique_ptr<Var[]> buf(new Var[2 * maxSize]);

	auto i = buf.get();
	Slice<Var> vars1(i, vars.size());
	i = copy(vars, i);
	Slice<Var> vars2(i, f.vars.size());
	i = copy(f.vars, i);
	sort(vars1);
	sort(vars2);

	Slice<Var> common(i,
		set_intersection(vars1, vars2, i));
	const vindex C = common.size();

	// Create the new result factor
	const vindex D = maxSize-C;
	const findex N = 1 << D;
	Factor result(D);

	// Copy Vars to the result factor
	auto notCommon = bind(notIn, _1, common);
	i = result.vars.begin();
	i = copy(common, i);
	i = copy_if(vars, i, notCommon);
	copy_if(f.vars, i, notCommon);

	// Calculate values for the result factor
	FVal vals1(facts, vars, common);
	FVal vals2(f.facts, f.vars, common);

	vindex v = 0;
	for (auto& fact : result.facts) {
		size_t mask = N - 1;
		size_t unique2 = v & mask;
		mask >>= D2-C;
		unique2 &= ~mask;
		unique2 >>= D1;
		size_t unique1 = v & mask;
		mask >>= D1-C;
		unique1 &= ~mask;
		unique1 >>= C;
		size_t common0 = v & mask;

		auto f1 = vals1(unique1, common0);
		auto f2 = vals2(unique2, common0);
		fact = f1 * f2;

		++v;
	}

	return result;
}

bool Factor::operator ==(Factor const&f) const
{
	return
	vars == f.vars &&
	facts == f.facts;
}

bool Factor::operator !=(Factor const&f) const
{
	return
	vars != f.vars ||
	facts != f.facts;
}

bool Factor::containsVar(Var var) const
{
	return find(vars, var) != vars.end();
}

Factor Factor::factorWithout(Var *without)
{
	if (without < vars.begin() ||
		without >= vars.end())
		return *this;

	Factor result(vars.size() - 1);
	auto i = begin(result.vars);
	i = copy(begin(vars), without, i);
	copy(++without, end(vars), i);

	return result;
}

Factor inference(
	Slice<Factor> factors_,
	Slice<Var> query,
	Slice<Var> hidden,
	Slice<Asst> evidence)
{
	unique_ptr<Factor[]> buf(new Factor[factors_.size()]);
	Slice<Factor> factors(buf.get(), factors_.size());
	copy(factors_, factors.begin());

	restrictFactors(factors, evidence);
	for (auto var : hidden) {
		factors = sumoutFactors(factors, var);
	}

	auto containsQuery = bind(containsAll, _1, query);
	auto i = find_if(factors, containsQuery);
	if (i == factors.end()) throw "Ahh!";
	Factor result = move(*i);
	while (i = find_if(++i, factors.end(), containsQuery),
		   i != factors.end()) {
		result = result * (*i);
	}

	debug("Inferred factor", result);
	return result;
}

static
ostream &operator <<(ostream &out, Slice<Var> vars)
{
	if (vars.empty()) return out;
	auto i = vars.begin();
	out << i->name();
	while (++i != vars.end()) {
		out << ", " << i->name();
	}
	return out;
}

static
void printVarList(ostream &out, findex i, Slice<Var> vars)
{
	if (vars.empty()) return;
	auto varit = vars.begin();
	findex bit = i & 1;
	i >>= 1;
	if (!bit) out << '~';
	out << varit->name();

	while (++varit != vars.end()) {
		out << ", ";
		bit = i & 1;
		i >>= 1;
		if (!bit) out << '~';
		out << varit->name();
	}
}

ostream &operator <<(ostream &out, Factor const&f)
{
	out << "f(" << f.vars << ")";
	findex i = 0;
	for (auto val : f.facts) {
		out << '\n';
		printVarList(out, i++, f.vars);
		out << "\t: " << val;
	}
	return out;
}

/// Utility functions ///

findex findexWith(findex f, const vindex v)
{
	const findex mask = (1 << v) - 1;
	findex lo = f & mask;
	findex hi = f & (~mask);
	return (hi << 1) | lo;
}

bool notIn(Var var, Slice<Var> slice)
{
	for (Var c : slice) {
		if (var == c) return false;
	}
	return true;
}

bool containsAll(Factor const&factor, Slice<Var> vars)
{
	for (auto var : vars) {
		if (!factor.containsVar(var))
			return false;
	}
	return true;
}

void restrictFactors(
	Slice<Factor> factors,
	Slice<Asst> evidence)
{
	if (evidence.empty()) return;
	for (auto &factor : factors) {
		const auto original = factor;
		for (auto asst : evidence) {
			factor = factor.restrict(asst);
			debug("Restricted factor", factor);
		}
	}
}

Slice<Factor> sumoutFactors(
	Slice<Factor> factors, Var var)
{
	const auto containsVar =
		bind(&Factor::containsVar, _1, var);

	// Find the first factor containing var
	auto i = find_if(factors, containsVar);
	if (i == factors.end()) return factors;
	auto &result = *i++;

	// Multiply with other factors containing var
	while (i = find_if(i, factors.end(), containsVar),
		   i != factors.end())
	{
		result = result * (*i);
		auto last = factors.end() - 1;
		iter_swap(i, last);
		factors = factors.subslice(last);
	}

	// Sumout the var
	result = result.sumout(var);
	debug("Summed out factor", result);

	// The var variable has been eliminated
	return factors;
}

FVal::FVal(
	Slice<double> vals,
	Slice<Var> vars,
	Slice<Var> common) :
indicies(new vindex[2*common.size()], common.size()),
sorted(indicies.end(), common.size()),
vals(vals)
{
	auto i = indicies.begin();
	auto s = sorted.begin();
	for (auto c : common) {
		vindex v = find(vars, c) - vars.begin();
		*i++ = *s++ = v;
	}
	sort(sorted);
}

FVal::~FVal()
{
	delete[] indicies.begin();
}

double FVal::operator ()(
	size_t uniqueBits,
	size_t commonBits)
{
	for (auto v : sorted) {
		vindex mask = (1 << v) - 1;
		vindex lo = uniqueBits & mask;
		vindex hi = uniqueBits & (~mask);
		uniqueBits = (hi << 1) | lo;
	}

	for (auto v : indicies) {
		vindex cbit = (commonBits & 1) << v;
		uniqueBits |= cbit;
		commonBits >>= 1;
	}

	return vals[uniqueBits];
}

#ifndef NDEBUG
#include <iostream>

void debug(char const *data, Factor const&factor)
{
	cerr << data << ": " << factor << '\n' << endl;
}

#else

void debug(char const *, Factor const&) {}

#endif // NDEBUG
