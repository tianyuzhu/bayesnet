#include "var.h"

Var::Var() :
name_(nullptr)
{}

Var::Var(char const*name_) :
name_(name_)
{}

Var::Var(Var const& var) :
name_(var.name_)
{}

Var &Var::operator =(Var const& var)
{
	name_ = var.name_;
	return *this;
}

bool Var::operator ==(Var const& var) const
{
	return name_ == var.name_;
}

bool Var::operator <(Var const& var) const
{
	return name_ < var.name_;
}

char const *Var::name() const
{
	return name_;
}

Asst Var::operator ~() const
{
	return Asst(*this, false);
}

Asst::Asst(Var const&var, bool val) :
var(var),
val(val)
{}
