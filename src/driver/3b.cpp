#include "decision.h"
#include "question.h"
#include <memory>

using namespace std;

int main()
{
	do_question<1>();
	return 0;
}

template<>
Factor answer<1>()
{
	Var query[] {B};
	Var hidden[] {Trav, FP, Fraud, IP, OC, CRP};
	Asst evidence[] {FP, ~IP, CRP};
	Factor r(inference(
		decisionFactors, query, hidden, evidence));
	r.normalize();
	return r;
}
