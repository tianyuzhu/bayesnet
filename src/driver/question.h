#ifndef QUESTION_H
#define QUESTION_H

#include <memory>
#include <iostream>

template<unsigned Part>
Factor answer();

template<unsigned Part>
void question()
{
	using namespace std;
	cout << ">>> Part " << Part << " <<<\n";
	Factor ans(answer<Part>());
	cout << ">>> Answer for part " << Part << " <<<\n";
	cout << ans << '\n' << endl;
}

template<unsigned Part>
void do_question()
{
	do_question<Part-1>();
	question<Part>();
}

template<>
void do_question<1>()
{
	question<1>();
}

#endif
