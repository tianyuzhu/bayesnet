#include "fraud.h"

Var Fraud("Fraud");
Var Trav("Trav");
Var FP("FP");
Var IP("IP");
Var OC("OC");
Var CRP("CRP");

static Var var_PFraud[] = {
	Trav,
	Fraud
};
static double val_PFraud[] = {
	0.996, // ~Fraud | ~Trav
	0.99,  // ~Fraud |  Trav
	0.004, //  Fraud | ~Trav
	0.01   //  Fraud |  Trav
};
Factor PFraud(var_PFraud, val_PFraud);

static Var var_PTrav[] = {
	Trav
};
static double val_PTrav[] = {
	0.95,  // ~Trav
	0.05   //  Trav
};
Factor PTrav(var_PTrav, val_PTrav);

static Var var_PForeignPurchase[] = {
	Fraud,
	Trav,
	FP
};
static double val_PForeignPurchase[] = {
	0.99,  // ~FP | ~Trav, ~Fraud
	0.9,   // ~FP | ~Trav,  Fraud
	0.1,   // ~FP |  Trav, ~Fraud
	0.1,   // ~FP |  Trav,  Fraud
	0.01,  //  FP | ~Trav, ~Fraud
	0.1,   //  FP | ~Trav,  Fraud
	0.9,   //  FP |  Trav, ~Fraud
	0.9    //  FP |  Trav,  Fraud
};
Factor PForeignPurchase(
	var_PForeignPurchase,
	val_PForeignPurchase);

static Var var_POwnsComputer[] = {
	OC
};
static double val_POwnsComputer[] = {
	0.35,  // ~OC
	0.65   //  OC
};
Factor POwnsComputer(var_POwnsComputer, val_POwnsComputer);

static Var var_PInternetPurchase[] = {
	OC,
	Fraud,
	IP
};
static double val_PInternetPurchase[] = {
	0.999, // ~IP | ~Fraud, ~OC
	0.99,  // ~IP | ~Fraud,  OC
	0.989, // ~IP |  Fraud, ~OC
	0.98,  // ~IP |  Fraud,  OC
	0.001, //  IP | ~Fraud, ~OC
	0.01,  //  IP | ~Fraud,  OC
	0.011, //  IP |  Fraud, ~OC
	0.02   //  IP |  Fraud,  OC
};
Factor PInternetPurchase(
	var_PInternetPurchase,
	val_PInternetPurchase);

static Var var_PComputerRelatedPurchase[] = {
	OC,
	CRP
};
static double val_PComputerRelatedPurchase[] = {
	0.999, // ~CRP | ~OC
	0.9,   // ~CRP |  OC
	0.001, //  CRP | ~OC
	0.1    //  CRP |  OC
};
Factor PComputerRelatedPurchase(
	var_PComputerRelatedPurchase,
	val_PComputerRelatedPurchase);

Factor factors[6] = {
	PFraud,
	PTrav,
	PForeignPurchase,
	POwnsComputer,
	PInternetPurchase,
	PComputerRelatedPurchase
};
