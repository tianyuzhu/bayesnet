#include "decision.h"

Var B("B");
Var U("U");

static Var var_Utility[] = {
	Fraud,
	B
};
static double val_Utility[] = {
	    5, // ~B | ~Fraud
	-1000, // ~B |  Fraud
	  -10, //  B | ~Fraud
	    0  //  B |  Fraud
};
Factor Utility(var_Utility, val_Utility);

static Var dependants_Block[] = { FP, IP, CRP };
static Var hidden_Block[] = { Trav, FP, Fraud, IP, OC, CRP };
static Factor factors_Block[] = {
	PFraud,
	PTrav,
	PForeignPurchase,
	POwnsComputer,
	PInternetPurchase,
	PComputerRelatedPurchase,
	Utility
};
Factor Block(B, dependants_Block, hidden_Block, factors_Block);

Factor decisionFactors[8] {
	PFraud,
	PTrav,
	PForeignPurchase,
	POwnsComputer,
	PInternetPurchase,
	PComputerRelatedPurchase,
	Block,
	Utility
};
