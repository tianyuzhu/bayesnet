#ifndef DECISION_H
#define DECISION_H

#include "fraud.h"

extern Var B;
extern Var U;

extern Factor Block;
extern Factor Utility;

extern Factor decisionFactors[8];

#endif
