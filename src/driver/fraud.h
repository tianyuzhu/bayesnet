#ifndef FRAUD_H
#define FRAUD_H

#include "var.h"
#include "factor.h"

extern Var Fraud;
extern Var Trav;
extern Var FP;
extern Var IP;
extern Var OC;
extern Var CRP;

extern Factor PFraud;
extern Factor PTrav;
extern Factor PForeignPurchase;
extern Factor POwnsComputer;
extern Factor PInternetPurchase;
extern Factor PComputerRelatedPurchase;

/**
 * Contains the above six factors in order
 */
extern Factor factors[6];

#endif
