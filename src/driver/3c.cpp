#include "decision.h"
#include "question.h"
#include <memory>

using namespace std;

int main()
{
	do_question<3>();
	return 0;
}

template<>
Factor answer<1>()
{
	Var query[] {B};
	Var hidden[] {Trav, FP, Fraud, IP, OC, CRP};
	Asst evidence[] {FP, ~IP, CRP, ~Trav};
	Factor r(inference(
		decisionFactors, query, hidden, evidence));
	r = r * Utility;
	r = r.sumout(B);
	r = r.sumout(Fraud);
	return r;
}

template<>
Factor answer<2>()
{
	Var query[] {B};
	Var hidden[] {Trav, FP, Fraud, IP, OC, CRP};
	Asst evidence[] {FP, ~IP, CRP, Trav};
	Factor r(inference(
		decisionFactors, query, hidden, evidence));
	r = r * Utility;
	r = r.sumout(B);
	r = r.sumout(Fraud);
	return r;
}

template<>
Factor answer<3>()
{
	Var query[] {B};
	Var hidden[] {Trav, FP, Fraud, IP, OC, CRP};
	Asst evidence[] {FP, ~IP, CRP};
	Factor r(inference(
		decisionFactors, query, hidden, evidence));
	r = r * Utility;
	r = r.sumout(B);
	r = r.sumout(Fraud);
	return r;
}
