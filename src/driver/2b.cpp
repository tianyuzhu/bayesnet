#include "fraud.h"
#include "question.h"
#include <memory>

using namespace std;

int main()
{
	do_question<2>();
	return 0;
}

template<>
Factor answer<1>()
{
	Var query[] {Fraud};
	Var hidden[] {Trav, FP, IP, OC, CRP};
	Asst evidence[] {};
	Factor r(inference(
		factors, query, hidden, evidence));
	r.normalize();
	return r;
}

template<>
Factor answer<2>()
{
	Var query[] {Fraud};
	Var hidden[] {Trav, FP, IP, OC, CRP};
	Asst evidence[] {FP, ~IP, CRP};
	Factor r(inference(
		factors, query, hidden, evidence));
	r.normalize();
	return r;
}
