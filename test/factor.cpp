#include "gtest/gtest.h"
#include <memory>
#include <ostream>

#include "factor.h"

using namespace std;

/// Test utilities ///

struct test_factor
{
	unique_ptr<Var[]> _vdata;
	unique_ptr<double[]> _fdata;
	Slice<Var> vars;
	Slice<double> facts;
};

const test_factor &reveal(Factor const&f)
{
	return *reinterpret_cast<test_factor const*>(&f);
}

Var const *vars_of(Factor const&f)
{
	return reveal(f).vars.begin();
}

double const *vals_of(Factor const&f)
{
	return reveal(f).facts.begin();
}

ostream &operator <<(ostream &out, Var var) {
	return out << var.name();
}


/// Factor test ///

TEST(Factor, Construct_D1)
{
	Var vars[] {"One"};
	double vals[] {0.1, 0.9};
	Factor f(vars, vals);

	EXPECT_EQ(vars[0], vars_of(f)[0]);
	EXPECT_FLOAT_EQ(vals[0], vals_of(f)[0]);
	EXPECT_FLOAT_EQ(vals[1], vals_of(f)[1]);
}

TEST(Factor, Construct_D2)
{
	Var vars[] {"One", "Two"};
	double vals[] {0.1, 0.3, 0.2, 0.4};
	Factor f (vars, vals);

	EXPECT_EQ(vars[0], vars_of(f)[0]);
	EXPECT_EQ(vars[1], vars_of(f)[1]);

	EXPECT_FLOAT_EQ(vals[0], vals_of(f)[0]);
	EXPECT_FLOAT_EQ(vals[1], vals_of(f)[1]);
	EXPECT_FLOAT_EQ(vals[2], vals_of(f)[2]);
	EXPECT_FLOAT_EQ(vals[3], vals_of(f)[3]);
}

TEST(Factor, RestrictNothing)
{
	Var vars[] {"One", "Two"};
	Var other("Three");
	double vals[]
	{
		0.1, 0.3, // 00 01
		0.2, 0.4  // 10 11
	};

	Factor f (vars, vals);

	Factor r(f.restrict(~other));

	ASSERT_EQ(f, r);
}

TEST(Factor, RestrictTrue)
{
	Var vars[] {"One", "Two"};
	double vals[]
	{
		0.1, 0.3, // 00 01
		0.2, 0.4  // 10 11
	};

	Factor f (vars, vals);

	Factor r(f.restrict(vars[0]));

	EXPECT_EQ(vars[1], vars_of(r)[0]);

	// Only the oddly indexed entries of vals should
	// be in r, since these end with the 0th bit
	// being 1 (true)
	EXPECT_FLOAT_EQ(vals[1], vals_of(r)[0]);
	EXPECT_FLOAT_EQ(vals[3], vals_of(r)[1]);
}

TEST(Factor, RestrictFalse)
{
	Var vars[] {"One", "Two"};
	double vals[]
	{
		0.1, 0.3, // 00 01
		0.2, 0.4  // 10 11
	};

	Factor f (vars, vals);

	Factor r(f.restrict(~vars[1]));

	EXPECT_EQ(vars[0], vars_of(r)[0]);

	// Only the entries of vals with index <2 should
	// be in r, since these have the 1th bit
	// being 0 (false)
	EXPECT_FLOAT_EQ(vals[0], vals_of(r)[0]);
	EXPECT_FLOAT_EQ(vals[1], vals_of(r)[1]);
}

TEST(Factor, Sumout_Nothing)
{
	Var vars[] {"One"};
	Var other("Two");
	double vals[]
	{
		0.6, 0.4
	};

	Factor f (vars, vals);
	Factor r(f.sumout(other));

	ASSERT_EQ(f, r);
}

TEST(Factor, Sumout_D1)
{
	Var vars[] {"One"};
	double vals[]
	{
		0.6, 0.4
	};

	Factor f (vars, vals);
	Factor r(f.sumout(vars[0]));

	EXPECT_FLOAT_EQ(1, vals_of(r)[0]);
}

TEST(Factor, Sumout_D2)
{
	Var vars[] {"One", "Two"};
	double vals[]
	{
		0.1, 0.3, // 00 01
		0.2, 0.4  // 10 11
	};

	Factor f (vars, vals);
	Factor r(f.sumout(vars[1]));

	EXPECT_EQ(vars[0], vars_of(r)[0]);

	EXPECT_FLOAT_EQ(0.3, vals_of(r)[0]);
	EXPECT_FLOAT_EQ(0.7, vals_of(r)[1]);
}

TEST(Factor, Sumout_D3)
{
	Var vars[] {"One", "Two", "Three"};
	double vals[]
	{
		1, 3, // 000 001
		2, 4, // 010 011
		6, 3, // 100 101
		0, 5  // 110 111
	};

	Factor f (vars, vals);
	Factor r(f.sumout(vars[1]));

	EXPECT_EQ(vars[0], vars_of(r)[0]);
	EXPECT_EQ(vars[2], vars_of(r)[1]);

	EXPECT_FLOAT_EQ(3, vals_of(r)[0]);
	EXPECT_FLOAT_EQ(7, vals_of(r)[1]);
	EXPECT_FLOAT_EQ(6, vals_of(r)[2]);
	EXPECT_FLOAT_EQ(8, vals_of(r)[3]);
}

TEST(Factor, NormalizeOver)
{
	Var vars[] {"One"};
	double vals[] {1, 9};
	Factor f (vars, vals);
	f.normalize();

	EXPECT_FLOAT_EQ(0.1, vals_of(f)[0]);
	EXPECT_FLOAT_EQ(0.9, vals_of(f)[1]);
}

TEST(Factor, NormalizeUnder)
{
	Var vars[] {"One"};
	double vals[] {0.25, 0.5};
	Factor f (vars, vals);
	f.normalize();

	EXPECT_FLOAT_EQ(1.0/3.0, vals_of(f)[0]);
	EXPECT_FLOAT_EQ(2.0/3.0, vals_of(f)[1]);
}

TEST(Factor, NormalizeEqual)
{
	Var vars[] {"One"};
	double vals[] {0.25, 0.75};
	Factor f (vars, vals);
	f.normalize();

	EXPECT_FLOAT_EQ(vals[0], vals_of(f)[0]);
	EXPECT_FLOAT_EQ(vals[1], vals_of(f)[1]);
}

TEST(Factor, NormalizeZero)
{
	Var vars[] {"One"};
	double vals[] {0, 0};
	Factor f (vars, vals);
	f.normalize();

	EXPECT_FLOAT_EQ(vals[0], vals_of(f)[0]);
	EXPECT_FLOAT_EQ(vals[1], vals_of(f)[1]);
}

TEST(Factor, Multiply_D1_D1)
{
	Var vars[] {"One"};
	double vals[]
	{
		2, 5
	};

	Factor f1(vars, vals), f2(vars, vals);
	Factor r(f1 * f2);

	EXPECT_EQ(vars[0], vars_of(r)[0]);

	EXPECT_FLOAT_EQ(2*2, vals_of(r)[0]);
	EXPECT_FLOAT_EQ(5*5, vals_of(r)[1]);
}

TEST(Factor, Multiply_D1_D2)
{
	Var A("A"), B("B");

	Var vars1[] = {A};
	double vals1[] = {1, 9};
	Factor f1(vars1, vals1);

	Var vars2[] = {A, B};
	double vals2[] = {6, 1, 4, 9};
	Factor f2(vars2, vals2);

	Factor r(f1 * f2);

	EXPECT_EQ(A, vars_of(r)[0]);
	EXPECT_EQ(B, vars_of(r)[1]);

	EXPECT_FLOAT_EQ(1*6, vals_of(r)[0]);
	EXPECT_FLOAT_EQ(9*1, vals_of(r)[1]);
	EXPECT_FLOAT_EQ(1*4, vals_of(r)[2]);
	EXPECT_FLOAT_EQ(9*9, vals_of(r)[3]);
}

TEST(Factor, Multiply_D2_D2)
{
	Var vars1[] {"One", "Two"};
	Var vars2[] {vars1[0], "Three"};
	double vals[]
	{
		1, 3, // 00 01
		2, 4  // 10 11
	};

	Factor f1(vars1, vals), f2(vars2, vals);
	Factor r(f1 * f2);

	EXPECT_EQ(vars1[0], vars_of(r)[0]);
	EXPECT_EQ(vars1[1], vars_of(r)[1]);
	EXPECT_EQ(vars2[1], vars_of(r)[2]);

	EXPECT_FLOAT_EQ(1*1, vals_of(r)[0]);
	EXPECT_FLOAT_EQ(3*3, vals_of(r)[1]);
	EXPECT_FLOAT_EQ(2*1, vals_of(r)[2]);
	EXPECT_FLOAT_EQ(4*3, vals_of(r)[3]);
	EXPECT_FLOAT_EQ(1*2, vals_of(r)[4]);
	EXPECT_FLOAT_EQ(3*4, vals_of(r)[5]);
	EXPECT_FLOAT_EQ(2*2, vals_of(r)[6]);
	EXPECT_FLOAT_EQ(4*4, vals_of(r)[7]);
}

TEST(Factor, Multiply_D3_D3)
{
	Var vars1[] {"One", "Two", "Three"};
	Var vars2[] {vars1[1], "Four", vars1[0]};

	double vals[]
	{
		1, 3, // 000 001
		2, 4, // 010 011
		6, 3, // 100 101
		0, 5  // 110 111
	};

	Factor f1(vars1, vals), f2(vars2, vals);
	Factor r(f1 * f2);

	EXPECT_EQ(vars1[0], vars_of(r)[0]);
	EXPECT_EQ(vars1[1], vars_of(r)[1]);
	EXPECT_EQ(vars1[2], vars_of(r)[2]);
	EXPECT_EQ(vars2[1], vars_of(r)[3]);

	EXPECT_FLOAT_EQ(1*1, vals_of(r)[ 0]);
	EXPECT_FLOAT_EQ(3*6, vals_of(r)[ 1]);
	EXPECT_FLOAT_EQ(2*3, vals_of(r)[ 2]);
	EXPECT_FLOAT_EQ(4*3, vals_of(r)[ 3]);
	EXPECT_FLOAT_EQ(6*1, vals_of(r)[ 4]);
	EXPECT_FLOAT_EQ(3*6, vals_of(r)[ 5]);
	EXPECT_FLOAT_EQ(0*3, vals_of(r)[ 6]);
	EXPECT_FLOAT_EQ(5*3, vals_of(r)[ 7]);
	EXPECT_FLOAT_EQ(1*2, vals_of(r)[ 8]);
	EXPECT_FLOAT_EQ(3*0, vals_of(r)[ 9]);
	EXPECT_FLOAT_EQ(2*4, vals_of(r)[10]);
	EXPECT_FLOAT_EQ(4*5, vals_of(r)[11]);
	EXPECT_FLOAT_EQ(6*2, vals_of(r)[12]);
	EXPECT_FLOAT_EQ(3*0, vals_of(r)[13]);
	EXPECT_FLOAT_EQ(0*4, vals_of(r)[14]);
	EXPECT_FLOAT_EQ(5*5, vals_of(r)[15]);
}

TEST(Factor, Inference2_NoEvidence)
{
	Var A("A"), B("B");

	Var vars1[] = {A};
	double vals1[] = {1, 9};
	Factor f1(vars1, vals1);

	Var vars2[] = {A, B};
	double vals2[] = {6, 1, 4, 9};
	Factor f2(vars2, vals2);

	Factor factors[] = {f1, f2};
	Var query[] = {B};
	Var hidden[] = {A};
	Asst evidence[] = {};
	Factor r(inference(
		factors, query, hidden, evidence));

	EXPECT_EQ(B, vars_of(r)[0]);

	EXPECT_FLOAT_EQ(15, vals_of(r)[0]);
	EXPECT_FLOAT_EQ(85, vals_of(r)[1]);
}

TEST(Factor, Inference3_NoEvidence)
{
	Var A("A"), B("B"), C("C");

	Var vars1[] = {A};
	double vals1[] = {1, 9};
	Factor f1(vars1, vals1);

	Var vars2[] = {A, B};
	double vals2[] = {6, 1, 4, 9};
	Factor f2(vars2, vals2);

	Var vars3[] = {B, C};
	double vals3[] = {8, 3, 2, 7};
	Factor f3(vars3, vals3);

	Factor factors[] = {f1, f2, f3};
	Var query[] = {C};
	Var hidden[] = {A, B};
	Asst evidence[] = {};
	Factor r(inference(
		factors, query, hidden, evidence));

	EXPECT_EQ(C, vars_of(r)[0]);

	EXPECT_FLOAT_EQ(375, vals_of(r)[0]);
	EXPECT_FLOAT_EQ(625, vals_of(r)[1]);
}

TEST(Factor, Inference4_NoEvidence)
{
	Var A("A"), B("B"), C("C"), D("D");

	Var vars1[] = {A};
	double vals1[] = {7, 3};
	Factor f1(vars1, vals1);

	Var vars2[] = {B};
	double vals2[] = {6, 4};
	Factor f2(vars2, vals2);

	Var vars3[] = {A, B, C};
	double vals3[] = {5, 8, 3, 1, 5, 2, 7, 9};
	Factor f3(vars3, vals3);

	Var vars4[] = {C, D};
	double vals4[] = {2, 4, 8, 6};
	Factor f4(vars4, vals4);

	Factor factors[] = {f1, f2, f3, f4};
	Var query[] = {D};
	Var hidden[] = {A, B, C};
	Asst evidence[] = {};
	Factor r(inference(
		factors, query, hidden, evidence));

	EXPECT_EQ(D, vars_of(r)[0]);

	EXPECT_FLOAT_EQ(3100, vals_of(r)[0]);
	EXPECT_FLOAT_EQ(6900, vals_of(r)[1]);
}

TEST(Factor, Inference4_Evidence2)
{
	Var A("A"), B("B"), C("C"), D("D");

	Var vars1[] = {A};
	double vals1[] = {7, 3};
	Factor f1(vars1, vals1);

	Var vars2[] = {B};
	double vals2[] = {6, 4};
	Factor f2(vars2, vals2);

	Var vars3[] = {A, B, C};
	double vals3[] = {5, 8, 3, 1, 5, 2, 7, 9};
	Factor f3(vars3, vals3);

	Var vars4[] = {C, D};
	double vals4[] = {2, 4, 8, 6};
	Factor f4(vars4, vals4);

	Factor factors[] = {f1, f2, f3, f4};
	Var query[] = {D};
	Var hidden[] = {A, B, C};
	Asst evidence[] = {B, ~A};
	Factor r(inference(
		factors, query, hidden, evidence));

	EXPECT_EQ(D, vars_of(r)[0]);

	EXPECT_FLOAT_EQ(34, vals_of(r)[0]);
	EXPECT_FLOAT_EQ(66, vals_of(r)[1]);
}
