#ifndef SLICE_ALGORITHM_H
#define SLICE_ALGORITHM_H

#include <algorithm>
#include "slice.h"

template<typename T, typename OutputIt>
OutputIt copy(Slice<T> slice, OutputIt dest)
{
	using namespace std;
	return copy(slice.begin(), slice.end(), dest);
}

template<typename T, typename OutputIt, typename UnaryPredicate>
OutputIt copy_if(Slice<T> slice, OutputIt dest, UnaryPredicate pred)
{
	using namespace std;
	return copy_if(slice.begin(), slice.end(), dest, pred);
}

template<typename T>
auto find(Slice<T> slice, const T& value)
-> decltype(slice.begin())
{
	using namespace std;
	return find(slice.begin(), slice.end(), value);
}

template<typename T, typename UnaryPredicate>
auto find_if(Slice<T> slice, UnaryPredicate pred)
-> decltype(slice.begin())
{
	using namespace std;
	return find_if(slice.begin(), slice.end(), pred);
}

template<typename T, typename OutputIt>
OutputIt set_intersection(
	Slice<T> set1, Slice<T> set2,
	OutputIt dest)
{
	using namespace std;
	return set_intersection(
		set1.begin(), set1.end(),
		set2.begin(), set2.end(),
		dest);
}

template<typename T>
void sort(Slice<T> slice)
{
	using namespace std;
	sort(slice.begin(), slice.end());
}

#endif
