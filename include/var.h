#ifndef VAR_H
#define VAR_H

class Asst;

class Var
{
public:
	Var();
	Var(char const *name);
	Var(Var const &var);

	Var& operator =(Var const& var);

	bool operator ==(Var const& var) const;
	bool operator !=(Var const& var) const;
	bool operator  <(Var const& var) const;

	Asst operator ~() const;

	char const *name() const;

private:
	char const *name_;
};

class Asst
{
	friend class Var;
public:
	Asst() = default;
	Asst(Var const&var, bool val=true);
	Asst(Asst const&asst) = default;

	Asst &operator =(Asst const&) = default;

	Var var;
	bool val;
};

#endif // VAR_H
