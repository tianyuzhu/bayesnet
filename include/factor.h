#ifndef FACTOR_H
#define FACTOR_H

#include "var.h"
#include "slice.h"
#include <memory>
#include <iosfwd>

class Factor
{
public:
	template<size_t D>
	Factor(
		Var const (&vars)[D],
		double const (&facts)[1<<D]);

	Factor(
		Var decision,
		Slice<Var> dependants,
		Slice<Var> hidden,
		Slice<Factor> factors);

	Factor(Factor const&); // Copyable (no ownership)
	Factor(Factor &&); // Moveable (moved loses ownership)

	Factor &operator =(Factor const&f); // Copy assignment
	Factor &operator =(Factor &&f);     // Move assignment

	Factor restrict(Asst asst);
	Factor sumout(Var var);
	void normalize();

	Factor operator *(Factor const&f);

	bool operator ==(Factor const&f) const;
	bool operator !=(Factor const&f) const;

	friend
	Factor inference(
		Slice<Factor> factors,
		Slice<Var> query,
		Slice<Var> hidden,
		Slice<Asst> evidence);

	friend
	std::ostream &operator <<(std::ostream&, Factor const&);

	bool containsVar(Var var) const;

private:
	Factor();
	explicit Factor(size_t);
	void init(size_t, Var const *, double const *);
	Factor factorWithout(Var*);

	std::unique_ptr<Var[]> _vdata;
	std::unique_ptr<double[]> _fdata;
	Slice<Var> vars;
	Slice<double> facts;
};

template<size_t D>
Factor::Factor(
	Var const (&vars_)[D],
	double const (&facts_)[1<<D]) :
_vdata(new Var[D]),
_fdata(new double[1<<D]),
vars(_vdata.get(), D),
facts(_fdata.get(), 1<<D)
{
	init(D, &vars_[0], &facts_[0]);
}

#endif // FACTOR_H
