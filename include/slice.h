#ifndef SLICE_H
#define SLICE_H

#include <cstddef>

template<typename T>
class Slice
{
public:
	Slice(T* begin, T* end);
	Slice(T* begin, size_t size);
	template<size_t N> Slice(T (&data)[N]);
	Slice(T (&data)[0]);

	T *begin();
	T const*begin() const;

	T *end();
	T const*end() const;

	bool empty() const;
	size_t size() const;

	T &operator [](size_t index);
	T const&operator [](size_t index) const;

	bool operator ==(Slice<T> const&s) const;
	bool operator !=(Slice<T> const&s) const;

	Slice<T> subslice(size_t end);
	const Slice<T> subslice(size_t end) const;
	Slice<T> subslice(size_t start, size_t end);
	const Slice<T> subslice(size_t start, size_t end) const;
	Slice<T> subslice(T *end);
	const Slice<T> subslice(T *end) const;
	Slice<T> subslice(T *start, T *end);
	const Slice<T> subslice(T *start, T *end) const;

private:
	T *begin_;
	T *end_;
};

template<typename T>
Slice<T>::Slice(T* begin, T* end) :
begin_(begin),
end_(end)
{}

template<typename T>
Slice<T>::Slice(T* begin, size_t size) :
Slice(begin, begin+size)
{}

template<typename T>
template<size_t N>
Slice<T>::Slice(T (&data)[N]) :
Slice(&data[0], N)
{}

template<typename T>
Slice<T>::Slice(T (&data)[0]) :
Slice(nullptr, nullptr)
{}

template<typename T>
T *Slice<T>::begin()
{
	return begin_;
}

template<typename T>
T const*Slice<T>::begin() const
{
	return begin_;
}

template<typename T>
T *Slice<T>::end()
{
	return end_;
}

template<typename T>
T const*Slice<T>::end() const
{
	return end_;
}

template<typename T>
bool Slice<T>::empty() const
{
	return end_ == begin_;
}

template<typename T>
size_t Slice<T>::size() const
{
	return end_ - begin_;
}

template<typename T>
T & Slice<T>::operator [](size_t index)
{
	T *result = begin_+index;
	assert(begin_ <= result && result < end_);
	return *result;
}

template<typename T>
T const& Slice<T>::operator [](size_t index) const
{
	T *result = begin_+index;
	assert(begin_ <= result && result < end_);
	return *result;
}

template<typename T>
bool Slice<T>::operator ==(Slice<T> const&s) const
{
	return
	begin_ == s.begin_ &&
	end_ == s.end_;
}

template<typename T>
bool Slice<T>::operator !=(Slice<T> const&s) const
{
	return
	begin_ != s.begin_ ||
	end_ != s.end_;
}

template <typename T>
Slice<T> Slice<T>::subslice(size_t end)
{
	return subslice(begin_ + end);
}

template <typename T>
Slice<T> Slice<T>::subslice(size_t start, size_t end)
{
	return subslice(begin_ + start, begin_ + end);
}

template <typename T>
Slice<T> Slice<T>::subslice(T *end)
{
	if (end_ < end || end < begin_) return *this;
	return Slice<T>(begin_, end);
}

template <typename T>
Slice<T> Slice<T>::subslice(T *start, T *end)
{
	if (end <= start) return Slice(nullptr, nullptr);
	if (start < begin_) start = begin_;
	if (end > end_) end = end_;
	return Slice<T>(start, end);
}

#endif
